class CommentsController < ApplicationController

  def new
    @article = Article.find(params[:article_id])
    @comment = Comment.new
  end

  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.build(comment_params)

    if @article.save
      redirect_to @article, notice: 'Comment was successfully created.'
    else
      render action: 'new'
    end
  end

  private
    def comment_params
      params.require(:comment).permit(:text, :article_id)
    end
end