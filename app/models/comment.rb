class Comment < ActiveRecord::Base

  belongs_to :article

  validates :text, :article_id, presence: true
end
